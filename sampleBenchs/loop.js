const array = createAndFillArray(20)

function loopFor() {
    for (let i = 0; i < array.length; i++) {
        array[i].toString()
    }
}

function loopForEach() {
    array.forEach(element => {
        element.toString()
    });
}
function loopForOf() {
    for (const element of array) {
        element.toString()
    }
}

function loopForIn() {
    for (const index in array) {
        array[index].toString()
    }
}
function createAndFillArray(length) {
    const array = []
    for (let i = 0; i < length; i++) {
        array.push(i)
    }
    return array
}

module.exports = [
    {
        title: 'Тест цикла for in',
        func: loopForIn
    },
    {
        title: 'Тест цикла for',
        func: loopFor
    },
    {
        title: 'Тест цикла for of',
        func: loopForOf
    },
    {
        title: 'Тест цикла forEach',
        func: loopForEach
    }
]