interface IMetric {
    time:number;
    memory:number;
    cpu:number;
}

export default IMetric