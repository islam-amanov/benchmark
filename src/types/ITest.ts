interface ITest {
    title: string,
    func: () => void
}

export default ITest;
