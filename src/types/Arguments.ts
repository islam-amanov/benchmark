interface NormalizedArgs {
    path: string;
    iterations: number;
    runs: number
}

// eslint-disable-next-line import/prefer-default-export
export { NormalizedArgs };
