import ITest from '../types/ITest';

function validateTests(tests: ITest[]) {
	tests.forEach((test) => isValidTest(test));
}

function isValidTest(test: ITest) {
	if (typeof test.title !== 'string') {
		throw new Error(`Ошибка в структуре теста - "${test.title}". Свойство "title" должно быть строкой.`);
	}

	if (typeof test.func !== 'function') {
		throw new Error(`Ошибка в структуре теста - "${test.title}". Свойство "func" должно быть функцией.`);
	}
}

export default validateTests;
