import { homedir } from "os";
import { isAbsolute, join, resolve } from "path";
import validateArgs from "./validateArgs";

function normalizeArgs(path: string, iterations: string, runs: string) {
  const normalizedPath = normalizePath(path);
  const normalizedIterations = normalizeIterations(iterations);
  const normalizedRuns = normalizeRuns(runs);

  const normalizedArgs = {
    path: normalizedPath,
    iterations: normalizedIterations,
    runs: normalizedRuns,
  };

  validateArgs(normalizedArgs);

  return normalizedArgs;
}

function normalizePath(path: string) {
  if (isHomedirFormat(path)) {
    return convertHomedirFormatToAbsolutePath(path);
  }

  if (!isAbsolute(path)) {
    return convertToAbsolutePath(path);
  }

  return path;
}

function isHomedirFormat(path: string) {
  if (path.startsWith("~")) {
    return true;
  }
  return false;
}
function convertHomedirFormatToAbsolutePath(path: string) {
  return join(homedir(), path.substring(1));
}
function convertToAbsolutePath(path: string): string {
  return resolve(process.cwd(), path);
}

function normalizeIterations(iterations: string) {
  return Number(iterations);
}

function normalizeRuns(runs: string) {
  return Number(runs);
}

export default normalizeArgs;
