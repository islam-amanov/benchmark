import { accessSync, constants, statSync } from 'fs';

import { parse } from 'path';
import { NormalizedArgs } from '../types/Arguments';

function validateArgs({ path, iterations, runs }: NormalizedArgs) {
	validatePath(path);
	validateIterations(iterations);
	validateRuns(runs);
}

function validatePath(path: string) {
	try {
		accessSync(path, constants.F_OK);
	} catch (error) {
		throw new Error('Убедитесь в том, что, по указанному пути существует файл');
	}

	if (!statSync(path).isFile()) {
		throw new Error('Путь должен указать к файлу');
	}

	if (!isJavaScriptFile(path)) {
		throw new Error('Укажите путь к JavaScript файлу');
	}
}

function validateIterations(iterations: number) {
	if (!isPositiveInteger(iterations)) {
		throw new Error('Второй аргумент(количество итерации) должен быть положительным целым числом');
	}
}

function validateRuns(runs: number) {
	if (!isPositiveInteger(runs)) {
		throw new Error('Третий аргумент(количество прогонов) должен быть положительным целым числом');
	}
}

function isJavaScriptFile(path: string) {
	return parse(path).ext === '.js';
}

function isPositiveInteger(num: number) {
	if (/^([1-9]|[0-9]+)$/.test(num.toString())) {
		return true;
	}
	return false;
}
export default validateArgs;
