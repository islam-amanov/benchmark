import ITest from "../types/ITest";
import validateTestFile from "./validateTestFile";

async function importTests(path: string): Promise<ITest[]> {
  const tests = (await import(path)).default;
  validateTestFile(tests);
  return tests;
}

export default importTests;
