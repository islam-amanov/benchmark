import ITest from '../types/ITest';
import validateTests from './validateTests';

function validateTestFile(tests: ITest[] | {}) {
	if (!Array.isArray(tests)) {
		throw new Error('Файл с тестами должен экспортировать массив.');
	}
	if (tests.length < 1) {
		throw new Error('Файл с тестами должен экспортировать не пустой массив');
	}
	validateTests(tests);
}

export default validateTestFile;
