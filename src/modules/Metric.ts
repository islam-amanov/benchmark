import IMetric from "../types/IMetric";

class Metric {
  private initialMetric: IMetric;
  private finalMetric: IMetric;
  private difference: IMetric;

  constructor() {
    this.initialMetric = { time: 0, memory: 0, cpu: 0 };
    this.finalMetric = { time: 0, memory: 0, cpu: 0 };
    this.difference = { time: 0, memory: 0, cpu: 0 };
  }

  fixInitialMetric() {
    const memory = process.memoryUsage().heapUsed;
    const cpu = process.cpuUsage().user;
    const time = Number(process.hrtime.bigint());
    this.initialMetric = this.convertToPrintFormat({ time, memory, cpu });
  }

  fixFinalMetric() {
    const time = Number(process.hrtime.bigint());
    const memory = process.memoryUsage().heapUsed;
    const cpu = process.cpuUsage().user;
    this.finalMetric = this.convertToPrintFormat({ time, memory, cpu });
  }

  getDifference() {
    const { initialMetric, finalMetric } = this;
    this.difference.time = finalMetric.time - initialMetric.time;
    this.difference.memory = finalMetric.memory - initialMetric.memory;
    this.difference.cpu = this.getPercentageDiff(
      initialMetric.cpu,
      finalMetric.cpu
    );
    return this.difference;
  }

  private convertToPrintFormat(metric: IMetric) {
    return {
      time: this.convertNanoToMilli(metric.time),
      memory: this.convertByteToKByte(metric.memory),
      cpu: metric.cpu,
    };
  }

  private convertNanoToMilli(num: number) {
    return num / 10 ** 6;
  }

  private convertByteToKByte(num: number) {
    return num / 1024;
  }

  private getPercentageDiff(numA: number, numB: number) {
    return (numB / numA - 1) * 100;
  }
}

export default Metric;