import IMetric from "../types/IMetric";

class TestResult {
  title: string;
  runs: IMetric[] = [];

  constructor(title: string) {
    this.title = title;
  }

  private getTotalMetric() {
    const reducer = (accumulator: IMetric, currentValue: IMetric) => {
      accumulator.time += currentValue.time;
      accumulator.memory += currentValue.memory;
      accumulator.cpu += currentValue.cpu;
      return accumulator;
    };
    const initialValue: IMetric = { time: 0, memory: 0, cpu: 0 };
    const totalMetric = this.runs.reduce(reducer, initialValue);
    return totalMetric;
  }

  get avg(): IMetric {
    const numberOfRuns = this.runs.length;
    let { time, memory, cpu } = this.getTotalMetric();
    return {
      time: time / numberOfRuns,
      memory: memory / numberOfRuns,
      cpu: cpu / numberOfRuns,
    };
  }
}

export default TestResult;