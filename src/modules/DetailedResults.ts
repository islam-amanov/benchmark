import { Table } from "console-table-printer";
import TestResult from "./TestResult";

class DetailedResults {
  private testResults: TestResult[];
  private bestTime: number;

  constructor(testResults: TestResult[]) {
    this.testResults = testResults;
    this.bestTime = this.findBestTime();
  }

  private getMin(num1: number, num2: number) {
    return num1 < num2 ? num1 : num2;
  }

  private findBestTime() {
    let bestTime = this.testResults[0].avg.time;
    this.testResults.forEach((result) => {
      const { time } = result.avg;
      bestTime = this.getMin(time, bestTime);
    });
    return bestTime;
  }

  print() {
    const table = new Table();
    this.testResults.forEach((result) => {
      const { title } = result;
      const { time, memory, cpu } = result.avg;
      const timeDiff = time - this.bestTime;
      const isBestTime = timeDiff === 0;
      const color = isBestTime ? "green" : "yellow";
      const row = {
        title,
        "Time(ms)": time,
        "TimeDiff(ms)": timeDiff,
        "Memory(KB)": memory,
        "CPU(%)": cpu,
      };
      table.addRow(row, { color });
    });

    table.printTable();
  }
}

export default DetailedResults;
