import ITest from "../types/ITest";
import TestResult from "./TestResult";
import DetailedResults from "./DetailedResults";
import sleep from "../utils/sleep";
import Metric from "./Metric";
import normalizeArgs from "../utils/normalizeArgs";
import importTests from "../utils/importTests";

class Benchmark {
  path: string;
  iterations: number;
  runs: number;

  testResults: TestResult[] = [];

  constructor(path:string, iterations:string, runs:string ) {
    const normalizedArgs = normalizeArgs(path,iterations,runs)
    this.path = normalizedArgs.path;
    this.iterations = normalizedArgs.iterations;
    this.runs = normalizedArgs.runs;
  }

  async start() {
    const tests = await importTests(this.path);
    await this.warmUp(tests);
    await this.iterateTests(tests);
  }

  async warmUp(tests: ITest[]) {
    for (let i = 0; i < tests.length; i++) {
      tests[i].func();
    }
    await sleep(1000);
  }

  async iterateTests(tests: ITest[]) {
    for (let i = 0; i < tests.length; i += 1) {
      const testResult = await this.runTest(tests[i]);
      this.testResults.push(testResult);
    }
  }

  async runTest(test: ITest): Promise<TestResult> {
    let testResult = new TestResult(test.title);
    const metric = new Metric();
    for (let i = 0; i < this.runs; i += 1) {
      metric.fixInitialMetric();
      this.iterateFunction(test.func);
      metric.fixFinalMetric();
      testResult.runs.push(metric.getDifference());
      await sleep(500);
    }
    return testResult;
  }

  iterateFunction(func: () => void) {
    for (let i = 0; i < this.iterations; i += 1) {
      func();
    }
  }

  printResults() {
    const detailedResults = new DetailedResults(this.testResults);
    detailedResults.print();
  }
}

export default Benchmark;
