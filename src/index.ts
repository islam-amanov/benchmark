import process from "process";
import Benchmark from "./modules/Benchmark";

// eslint-disable-next-line func-names
(async function () {
  try {
    const path = process.argv[2];
    const iterations = process.argv[3];
    const runs = process.argv[4];
    const benchmark = new Benchmark(path, iterations, runs);
    await benchmark.start();
    benchmark.printResults();
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
})();
