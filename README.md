
# Benchmark

**Benchmark**  эта консольная утилита для тестирования производительности функции.


## Инструкция по запуску

1. Склонируйте данный репозиторий выполняя следующую команду в терминале(в командной строке, если у вас windows):

    ```bash
    git clone https://amanovofficial@bitbucket.org/islam-amanov/benchmark.git
    ```
2. Перейдите в директорию benchmark выполнив следующую команду:
    
    ```bash
    cd benchmark
    ```
3. Установите зависимости проекта:
    ```bash
    npm install
    ```
4. Скомпилируйте typescript код на javascript:
    ```bash
    npm run compile
    ```
5. Запустите тестирования ваших функции:
    ```bash
    ./dist/index.js path iterations runs
    ```
    где, **path** - путь к тестируемому файлу. Путь может быть абсолютным, относительным, а также через домашний каталог(например: ~/benchmarks/func1.js),  **iterations** - количество итерации для цикла  for(целое положительное число), **runs** - количество прогонов(целое положительное число) 



## Требование к формату экспорта тестируемых функции
1. **path** - должен указать к JavaScript файлу. Файл должен экспортировать функции подлещажие тестированию в следующем формате:
    ```javascript
        function someFunc(){
            //do something
        }
        function someFunc2(){
            //do something
        }

        module.exports = [
            {
                name:'Тест фунций someFunc',
                func: someFunc
            },
            {
                name:'Тест фунций someFunc2',
                func: someFunc2
            },
            //и т.д
        ]
    ```
    даже если файл содержит всего одну функцию, всеравно необходимо экспортировать ее внутри массива:
     ```javascript
        function someFunc(){
            //do something
        }
       
        module.exports = [
            {
                name:'Тест фунций someFunc',
                func: someFunc
            }
        ]
    ```

    ## Примеры
    В корне проекта есть папка sampleBenchs, в котором храниться файл loop.js c тестовыми функциями, Содержимое файла loop.js:
    ```javascript
    function loopFor(numberOfIterations) {
        const array = createAndFillArray(numberOfIterations)
        for (let i = 0; i < array.length; i++) {
            array[i].toString()
        }
    }

    function loopForEach(numberOfIterations) {
        const array = createAndFillArray(numberOfIterations)
        array.forEach(element => {
            element.toString()
        });
    }
    function loopForOf(numberOfIterations) {
        const array = createAndFillArray(numberOfIterations)
        for (const element of array) {
            element.toString()
        }
    }

    function loopForIn(numberOfIterations) {
        const array = createAndFillArray(numberOfIterations)
        for (const index in array) {
            array[index].toString()
        }
    }

    function createAndFillArray(length){
        const array = []
        for (let i = 0; i < length; i++) {
            array.push(i)
        }
        return array
    }
    module.exports = [
        {
            name: 'Тест цикла for in',
            func: loopForIn
        },
        {
            name: 'Тест цикла for of',
            func: loopForOf
        },
        {
            name: 'Тест цикла forEach',
            func: loopForEach
        },
        {
            name: 'Тест цикла for',
            func: loopFor
        }
    ]
    ```

    Проверим вышеуказанныe функции на производительность, с помощью нашей утилиты. Для этого откроем терминал( или командную строку) в корне проекта и выполним следующую команду:
    ```bash
    node ./dist/index.js ./sampleBenchs/loop.js 1000 5
    ```
    Данная команда запустит утилиту benchmark и передает ей три аргумента:
    1. **./sampleBenchs/loop.js** - путь к файлу содержащую тестируемых функции
    2. **1000** - количество итерации
    3. **5** - количество прогонов.

    После запуска вышеуказанной команды, через определенное время появиться результат теста.
    ![A cute kitten](./public/images/bench_results.png)


    ## Связь с разработчиком
   Отправляйте сообщения об ошибках или предложениях в мою почту - amanovofficial@yandex.ru.